#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

if __name__ == "__main__":
    if (len(sys.argv) == 1):
        print( "Print info about file with train or test data. Provide list of filename as input arguments.")
	sys.exit(2)

    for i in xrange(1, len(sys.argv)):
      inFile = sys.argv[i]
      open_file = open(inFile, "rt")
      lines = open_file.readlines()
      nonEmptyLines = [l for l in lines if len(l.strip()) != 0]
      open_file.close()

      print "===================================================="
      print "Input file: '%s'" % (inFile)
      print "Number of samples in input: ", len(nonEmptyLines) - 1
      print "===================================================="
