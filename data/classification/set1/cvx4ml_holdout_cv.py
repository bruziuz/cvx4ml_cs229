#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

if __name__ == "__main__":
    if (len(sys.argv) == 1):
        print( "Holdout cross validation 70%/30%. Provide in first argument name of input file.\n")
	sys.exit(2)
    inFile = sys.argv[1]
    open_file = open(inFile, "rt")
    fistline = open_file.readline()
    lines = open_file.readlines()
    open_file.close()

    print "===================================================="
    print "Input file: '%s'" % (inFile)
    print "Number of samples in input: ", len(lines)

    k = 0.7
    trainSamplesCount = int(k * len(lines))

    outFile = open(inFile + ".train", "wb")
    outFile.write(fistline)
    trainSamples = lines[:trainSamplesCount]
    for line in trainSamples:
        outFile.write(line)
    outFile.close()

    outFile = open(inFile + ".test", "wb")
    outFile.write(fistline)
    testSamples = lines[trainSamplesCount:]
    for line in testSamples:
        outFile.write(line)
    outFile.close()

    print "Output train file: '%s'" % (inFile + ".train")
    print "Number of samples in train: ", len(trainSamples)
    print "Output test file: '%s'" % (inFile + ".test")
    print "Number of samples in test: ", len(testSamples)
    print "===================================================="
