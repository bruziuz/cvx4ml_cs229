#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

if __name__ == "__main__":
    if (len(sys.argv) == 1):
        print( "Read top n lines from files and print them to stdout. Provide in first argument name of input file and second number of samples.")
	sys.exit(2)

    inFile = sys.argv[1]
    numberOfSamples = int(sys.argv[2])

    open_file = open(inFile, "rt")
    fistline = open_file.readline()
    lines = open_file.readlines()
    open_file.close()

    print "===================================================="
    print "Input file: '%s'" % (inFile)
    print "Number of samples in input: ", len(lines)
    print "Request number of samples to get: ", int(numberOfSamples) 
    print "===================================================="

    trainSamples = lines[:numberOfSamples]
    outFname = inFile + ".%i_top" % (len(trainSamples))

    outFile = open(outFname, "wb")
    outFile.write(fistline)
    for line in trainSamples:
        outFile.write(line)
    outFile.close()

    print "Output file: '%s'" % (outFname)
    print "Number of samples in output: ", len(trainSamples)
    print "===================================================="
