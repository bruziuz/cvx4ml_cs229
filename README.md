# README #

This is repository with public available things relative to my project in CS229, 2017, Autumn (http://cs229.stanford.edu/)

Structure of repository:
=============================================================================================================
data                -- test data

derivations         -- several math derivations 

presentation/poster -- poster for project

presentation/final_report -- final report fot the project

binaries -- to launch binary goto directory with binary and launch with "learn --demo" arguments. 
Test data will getted from "./../../data" relative to current working directory.

Binaries have been built with with MS Visual Studio 2013 Version 12 Update 5.

Konstantin Burlachenko, 2017 (burlachenkok@gmail.com, bruziuz@stanford.edu)

Various links relative to thi project in my home page: https://sites.google.com/site/burlachenkok/articles/cvx4ml

===============================================================================================================
